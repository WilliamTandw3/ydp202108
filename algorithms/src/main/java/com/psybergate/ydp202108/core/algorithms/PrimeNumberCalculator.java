package com.psybergate.ydp202108.core.algorithms;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrimeNumberCalculator {

  public static void main(String[] args) {

    int length = 2000000;
    List<Integer> primes = new ArrayList<>();
    primes.add(2);
    for (int i=3; i<length; i+=2) {
      if (isPrime(i)) primes.add(i);
    }

    System.out.println(primes);
    System.out.println(primes.size());

  }

  public static boolean isPrime(int num) {
    if (num < 2) return false;
    if (num == 2) return true;
    if (num == 3) return true;
    if (num%2 == 0) return false;
    if (num%3 == 0) return false;
    for (int i=5; i<=Math.sqrt(num); i++) {
      if (num%i == 0) return false;
    }
    return true;
  }
}
