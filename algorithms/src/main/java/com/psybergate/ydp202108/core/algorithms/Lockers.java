package com.psybergate.ydp202108.core.algorithms;

public class Lockers {

  public static final int LENGTH = 1000;

  public static void main(String[] args) {

    boolean[] doors = new boolean[LENGTH];

    int x=0;
    for (int i=1; i<=LENGTH; i++) {
      for (int j=i; j<LENGTH; j=j+i) {
        doors[j] = !doors[j];
      }
      if (doors[i-1]) x++; // count open doors
    }
    System.out.println("Doors open: " + x);

  }

}
