package com.psybergate.javafnds.new_proj.core;
import com.psybergate.javafnds.abstracts.core.*;
import java.util.Scanner;

public class Order {
    public static void main(String[] args) {
        DoubleLinkedList<String> list = new DoubleLinkedList<String>();
        Scanner scanner = new Scanner(System.in);
        String input = "William";
        while (input != "exit") {
            list.insert(input);
            list.print();
            input = scanner.next();
        }
        list.print();
    }
}
