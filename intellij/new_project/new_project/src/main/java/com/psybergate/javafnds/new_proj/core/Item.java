package com.psybergate.javafnds.new_proj.core;

public class Item {
    private int number;

    public int getNumber() {
        return number;
    }

    public Item(int num) {
        this.number = num;
    }
}
