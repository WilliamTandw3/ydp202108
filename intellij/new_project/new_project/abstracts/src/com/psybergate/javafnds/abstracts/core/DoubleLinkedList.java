package com.psybergate.javafnds.abstracts.core;

public class DoubleLinkedList<T extends Comparable<T>> {
    public DLLNode<T> head;

    public DoubleLinkedList() {
        this.head = null;
    }

    public void insert(T _val) {
        DLLNode<T> newNode,curr;

        //If list is empty
        if (head == null) {
            newNode = new DLLNode<T>(_val);
            newNode.next = null;
            head = newNode;
            return;
        }

        curr = head;
        while (curr != null) {
            if (head.value.compareTo(_val) >= 0) {
                newNode = new DLLNode<>(_val);
                newNode.next = head;
                head = newNode;
                return;
            }

            //If we reach end of list
            if (curr.next == null) {
                newNode = new DLLNode<>(_val);
                curr.next = newNode;
                return;
            }

            //if curr < _val <= next
            if (curr.value.compareTo(_val) < 0 && curr.next.value.compareTo(_val) >= 0) {
                newNode = new DLLNode<>(_val);
                newNode.next = curr.next;
                curr.next = newNode;
                return;
            }
            curr = curr.next;
        }
    }



    public void print() {
        DLLNode<T> curr = head;
        System.out.print("[");
        if (curr != null) {
            do {
                if (curr.next == null) {
                    System.out.print(curr.value);
                } else {
                    System.out.print(curr.value+", ");
                }
                curr = curr.next;
            } while (curr != null);
        }
        System.out.println("]");
    }

    public void printReverse() {

    }
}
