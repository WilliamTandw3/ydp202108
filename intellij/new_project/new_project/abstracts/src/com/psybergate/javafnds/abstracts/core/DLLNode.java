package com.psybergate.javafnds.abstracts.core;

public class DLLNode<T> {
    public T value;
    public DLLNode<T> next;

    public DLLNode(T _val) {
        this.value = _val;
        this.next = null;
    }
}
