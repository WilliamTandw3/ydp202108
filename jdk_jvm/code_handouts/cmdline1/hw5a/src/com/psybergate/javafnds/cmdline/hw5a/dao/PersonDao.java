package com.psybergate.javafnds.cmdline.hw5a.dao;

import com.psybergate.javafnds.cmdline.hw5a.domain.Person;

/**
 * 
 * @since 19 Jan 2020
 */
public class PersonDao {

/**
 * Returns a Person object with the associated taxReferenceNum
 * This method returns immediately
 * @param taxReferenceNum A tax reference number used to be searched
 * @return A new person object with a name and reference number
 */
  public Person getPerson(String taxReferenceNum) {
    return new Person(taxReferenceNum, "John", "Smith", 200000);
  }

}
