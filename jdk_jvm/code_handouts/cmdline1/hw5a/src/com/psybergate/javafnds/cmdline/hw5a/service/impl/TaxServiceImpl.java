package com.psybergate.javafnds.cmdline.hw5a.service.impl;

import com.psybergate.javafnds.cmdline.hw5a.dao.PersonDao;
import com.psybergate.javafnds.cmdline.hw5a.domain.Person;
import com.psybergate.javafnds.cmdline.hw5a.domain.TaxTables;
import com.psybergate.javafnds.cmdline.hw5a.service.TaxService;

/**
 * 
 * @since 19 Jan 2020
 */
public class TaxServiceImpl
    implements TaxService {

  private PersonDao personDao = new PersonDao();

  public int calculateTax(String taxReferenceNum) {
    Person person = personDao.getPerson(taxReferenceNum);
    int taxRate = TaxTables.getInstance().getTaxRate(person.getSalary());
    return taxRate * person.getSalary();
  }

}
