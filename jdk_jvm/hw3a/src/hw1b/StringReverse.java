package hw1b; 
import hw1a.MyStringUtils;
import java.util.Scanner;

public class StringReverse {
	public static void reverse(String str) {
		System.out.println(MyStringUtils.reverse(str));
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter name: ");
		String name = scanner.nextLine();
		reverse(name);
	}
}