package pack1;
import pack2.ClassB;

public class ClassA {
	public static void main(String[] args) {
		ClassB.doSomething();
		
	}

	public static void doSomething() {
		System.out.println("Class A is doing something...");
	}
}