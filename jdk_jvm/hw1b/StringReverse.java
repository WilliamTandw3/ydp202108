package hw1b; 
import hw1a.MyStringUtils;

public class StringReverse {
	public static void reverse(String str) {
		System.out.println(MyStringUtils.reverse(str));
	}

	public static void main(String[] args) {
		reverse(args[0]);
	}
}