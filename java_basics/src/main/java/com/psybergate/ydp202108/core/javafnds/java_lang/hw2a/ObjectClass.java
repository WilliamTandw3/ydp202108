package com.psybergate.ydp202108.core.javafnds.java_lang.hw2a;

public class ObjectClass {

  public static final int NUMBER_OF_THINGS;

  protected String name;

  static {
    System.out.println("Initializing statically");
    NUMBER_OF_THINGS = 4;
//    throw new RuntimeException();     // Initializer must be able to complete normally
  }

  {
    System.out.println("Initializing name");
    name = "Shoelace";
  }
}
