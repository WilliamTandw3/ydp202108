package com.psybergate.ydp202108.core.javafnds.java_lang.hw4a;

public class Operators {

  public static void main(String[] args) {
    int num = 6;
    //mod
    System.out.println("(num%4) = " + (num % 4));

    //++num
    System.out.println("++num = " + ++num);

    //num--
    System.out.println("num-- = " + num--);
    System.out.println("num (after) = " + num);

    //==
    System.out.println("(num == 6) = " + (num == 6));
    String obj1 = new String("Khabzela");
    String obj2 = new String("Khabzela");
    String s1 = "Khabzela";
    String s2 = "Khabzela";
    int three = new Integer(3);
    int anotherThree = new Integer(3);
    System.out.println("(obj1 == obj2) = " + (obj1 == obj2));
    System.out.println("(s1 == s2) = " + (s1 == s2));
    System.out.println("(three == anotherThree) = " + (three == anotherThree));

    //&& & &
    if (false & LongIsTrue()) {     //prints
    }

    if (false && isTrue()) {        //does not print
    }

    if (false | LongIsTrue()) {     //prints
    }

    if (false || isTrue()) {        //prints
    }

    //+=
    num = 4;
    num += 2;
    System.out.println("num = " + num);

    //ternary operator
    boolean isTrue = true;
    System.out.println((isTrue ? "yes, it is true" : "no, it is false"));

    //switch
    num = 3;
    switch (num) {
      case 1:
        System.out.println("num is 1");
        break;
      case 2:
        System.out.println("num is 2");
        break;
      case 3:
        System.out.println("num is 3");
        break;
      default:
        System.out.println("num is greater than 3 or less than 1");
        break;
    }

  }

  public static boolean isTrue() {
    System.out.println("isTrue will return true");
    return true;
  }
  public static boolean LongIsTrue() {
    System.out.println("LongIsTrue will return true");
    return true;
  }
}
