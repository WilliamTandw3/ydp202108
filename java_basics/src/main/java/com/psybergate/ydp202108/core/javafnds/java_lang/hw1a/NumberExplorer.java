package com.psybergate.ydp202108.core.javafnds.java_lang.hw1a;

public class NumberExplorer {

  public static void main(String[] args) {
    //Largest values
    byte myByte = 0x7f;
    short myShort = 0x7fff;
    int myInt = 0x7fffffff;
    long myLong = 0x7fffffffffffffffl;

    System.out.println("myByte = " + myByte);
    System.out.println("myShort = " + myShort);
    System.out.println("myInt = " + myInt);
    System.out.println("myLong = " + myLong);

    myByte = (byte) 0x80;
    myShort = (short) 0x8000;
    myInt = 0x80000000;
    myLong = 0x8000000000000000l;

    System.out.println("myByte = " + NumberRetriever.SMALLEST_BYTE);
    System.out.println("myShort = " + NumberRetriever.SMALLEST_SHORT);
    System.out.println("myInt = " + NumberRetriever.SMALLEST_INT);
    System.out.println("myLong = " + NumberRetriever.SMALLEST_LONG);

  }

}

class NumberRetriever {
  public static final byte SMALLEST_BYTE = (byte) 0x80;
  public static final short SMALLEST_SHORT = (short) 0x8000;
  public static final int SMALLEST_INT = 0x80000000;
  public static final long SMALLEST_LONG = 0x8000000000000000l;
  public static final byte LARGEST_BYTE = 0x7f;
  public static final short LARGEST_SHORT = 0x7fff;
  public static final int LARGEST_INT = 0x7fffffff;
  public static final long LARGEST_LONG = 0x7fffffffffffffffl;
}
